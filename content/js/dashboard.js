/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.9200482430629213, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.7584269662921348, 500, 1500, "getCountCheckInOutChildren"], "isController": false}, {"data": [0.9999742035341158, 500, 1500, "getLatestMobileVersion"], "isController": false}, {"data": [0.8241174632927211, 500, 1500, "findAllConfigByCategory"], "isController": false}, {"data": [0.5126353790613718, 500, 1500, "getNotifications"], "isController": false}, {"data": [0.0, 500, 1500, "getHomefeed"], "isController": false}, {"data": [0.6568525273579989, 500, 1500, "me"], "isController": false}, {"data": [0.9191328934967012, 500, 1500, "getAllClassInfo"], "isController": false}, {"data": [0.6579084287200833, 500, 1500, "findAllChildrenByParent"], "isController": false}, {"data": [0.803480905711389, 500, 1500, "findAllSchoolConfig"], "isController": false}, {"data": [0.0, 500, 1500, "getChildCheckInCheckOut"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 58454, 0, 0.0, 256.4692920929279, 9, 24533, 32.0, 620.0, 1244.9000000000015, 2453.980000000003, 192.52989031981818, 478.35528232148147, 212.98254300850596], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["getCountCheckInOutChildren", 2492, 0, 0.0, 601.426565008026, 67, 2517, 394.5, 1320.4000000000005, 1811.4499999999994, 2207.6300000000015, 8.28187625042373, 5.2327870449454625, 10.247204305944207], "isController": false}, {"data": ["getLatestMobileVersion", 38765, 0, 0.0, 38.28523152328134, 9, 1070, 25.0, 62.0, 100.0, 202.0, 129.25156458910573, 86.46222826517327, 95.29778443825667], "isController": false}, {"data": ["findAllConfigByCategory", 3201, 0, 0.0, 468.1699468915966, 33, 2462, 279.0, 1111.8000000000002, 1470.8999999999996, 2103.98, 10.640101315303996, 12.032458323361354, 13.819662841166322], "isController": false}, {"data": ["getNotifications", 1385, 0, 0.0, 1083.2375451263547, 79, 2948, 950.0, 2241.8, 2398.1000000000004, 2563.080000000002, 4.6028886865316485, 39.16500891186548, 5.119814662069871], "isController": false}, {"data": ["getHomefeed", 75, 0, 0.0, 20171.920000000002, 7329, 24533, 20920.0, 23079.0, 23543.8, 24533.0, 0.24702743651394882, 4.320326523953427, 1.2361021334936266], "isController": false}, {"data": ["me", 1919, 0, 0.0, 781.6315789473684, 70, 2568, 565.0, 1887.0, 2155.0, 2440.0, 6.377618845049452, 8.40444949210026, 20.210325343931125], "isController": false}, {"data": ["getAllClassInfo", 5305, 0, 0.0, 282.19415645617346, 19, 2294, 173.0, 663.0, 926.6999999999998, 1607.6399999999976, 17.641933595828455, 10.199242860088326, 45.2935970931963], "isController": false}, {"data": ["findAllChildrenByParent", 1922, 0, 0.0, 780.4089490114466, 65, 2590, 564.5, 1959.9000000000003, 2174.85, 2414.77, 6.3879712042754875, 7.173991098551573, 10.20578211933076], "isController": false}, {"data": ["findAllSchoolConfig", 2959, 0, 0.0, 506.5427509293674, 39, 2515, 328.0, 1107.0, 1495.0, 2094.2000000000007, 9.83517195762799, 214.4912696853011, 7.213099746268186], "isController": false}, {"data": ["getChildCheckInCheckOut", 431, 0, 0.0, 3483.0324825986086, 1853, 5350, 3453.0, 4478.4, 4667.799999999999, 4970.240000000001, 1.430344543783145, 95.32883210895342, 6.581819814752128], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 58454, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
